<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\database\common
{
	type DBRecord		=Map<string,mixed>;
	type DBVector		=Vector<DBRecord>;
	type DBQuery		=Map<string,mixed>;
	type DBFields		=Vector<string>;
	type DBCondition	=Map<string,mixed>;
	type DBOrder		=Map<string,mixed>;
	type DBOptions		=Map<string,mixed>;
}
