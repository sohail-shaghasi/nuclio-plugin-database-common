<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/

namespace nuclio\plugin\database\common
{
	interface CommonInterface extends CommonQueryInterface
	{
		
		// /**
		//  * Count Colum of query result.
		//  * @param  String $sql
		//  * @return Mixed if yes return int columCount else return false;
		//  */
		// public function columnCount(string $sql):mixed;
		// 
		/**
		 * Using BeginTransaction For Commit
		 * @return Void
		 */
		public function beginTransaction():void;
		/**
		 * Using Commit apply changes
		 * @return Void
		 */
		public function commit():void;
		/**
		 * Using Commit apply changes
		 * @return Void
		 */
		public function rollBack():void;
	}
}
