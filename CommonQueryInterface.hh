<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/

namespace nuclio\plugin\database\common
{
	require_once('types.hh');
	
	use nuclio\plugin\database\orm\Model;
	
	interface CommonQueryInterface
	{
		/**
		* Finds records based on a query.
		* @param  string $query define
		* @return mixed result of query
		*/
		public function find(string $target, DBQuery $query, DBOptions $options=Map{}):CommonCursorInterface;
		
		/**
		* Finds a single record based on a query.
		* @param  string $query define
		* @return mixed result of query
		*/
		public function findOne(string $target, DBQuery $query, DBOptions $options=Map{}):?DBRecord;
		
		/**
		 * Finds a single record based on ID.
		 * @param  String $collection define table.
		 * @param  Mixed $id The id of the record to find.
		 * @param  Array $conditional define where, or condinal.
		 * @return Mixed Return rows or false
		 */
		public function findById(string $target, mixed $id, DBOptions $options=Map{}):?DBRecord;
		
		/**
		 * Updates an existing record or inserts it if no ID is provided or the ID doesn't exist.
		 * @param  String $collection define table.
		 * @param  Array $record The record to upsert.
		 * @param  Array $conditional define where, or condinal.
		 * @return Mixed Return rows or false
		 */
		public function upsert(string $target, DBRecord $record, DBQuery $condition=Map{}, DBOptions $options=Map{}):DBRecord;
		/**
		 * Deletes a record or many records by query.
		 * @param  String $collection define table.
		 * @param  string $query define
		 * @param  Boolean $justOne Delete one record or many.
		 * @return Boolean
		 */
		public function delete(string $target, DBQuery $query, bool $justOne=false):bool;
		
		
		
		public function distinct(string $target, string $field):Vector<string>;
		
		
		
		public function listCollections():Vector<mixed>;
		
		
		
		public function listCollectionFields(string $collection):Vector<Pair<string,string>>;
		
		public function buildColumn(string $name,Map<string,mixed> $annotations):string;
		
		public function collectionExists(string $target):bool;
		public function columnExists(Model $model,string $column):bool;
	}
}
