<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/

namespace nuclio\plugin\database\common
{
	// use \SeekableIterator;
	use \Iterator;
	
	interface CommonCursorInterface extends Iterator<?DBRecord>// extends SeekableIterator<Tv>
	{
		public function current():?DBRecord;
		
		public function key():mixed;
		
		public function next():void;
		
		public function rewind():void;
		
		public function valid():bool;
		
		public function seek(int $position):void;
		
		public function count():int;
		
		public function limit(int $limit):this;
		
		public function offset(int $offset):this;
		
		public function orderBy(DBOrder $order):this;
	}
}
