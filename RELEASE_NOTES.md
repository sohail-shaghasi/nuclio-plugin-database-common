Release Notes
-------------
2.1.3
-----
* Added some missing DBOptions type bindings.

-------------
2.1.2
-----
* Added some missing DBOptions type bindings.

2.1.1
-----
* Params on query() now defaults to an empty vector.

2.1.0
-----
* Introduced new type DBOptions
* Cleaned up PDO class to use the new type.

2.0.2
-----
* Fixed reference to Model in CommonQueryInterface.

2.0.1
-----
* Fixed syntax error.

2.0.0
-----
* [BC Break] Added new methods to CommonQueryInterface: buildColumn, collectionExists, columnExists.

1.0.2
-----
* Fixed namespace in PSR4 Reference.


1.0.1
-----
* Added copyright header.


1.0.0
-----
* Initial Release.

